package se.experis.notisboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.notisboard.models.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    Comment getById(Integer id);
}
