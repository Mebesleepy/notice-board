package se.experis.notisboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.notisboard.models.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {
    Post getById(Integer id);
}
