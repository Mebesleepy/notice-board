package se.experis.notisboard.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.notisboard.models.Author;

public interface UserRepository extends JpaRepository<Author, Integer> {
    Author getById(Integer id);
}
