package se.experis.notisboard.models;

public class PostDisplayModel {
    public String authorId;
    public String postId;
    public String authorName;
    public String postText;
    public String postDate;
    public int commentsAmount;
}
