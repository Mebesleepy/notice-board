package se.experis.notisboard.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(nullable = false)
    public String text;
    @Column(nullable = false)
    public String authorId;
    @Column
    public String date;

    @OneToMany(mappedBy="post", orphanRemoval = true)
    public List<Comment> comments = new ArrayList<>();
}