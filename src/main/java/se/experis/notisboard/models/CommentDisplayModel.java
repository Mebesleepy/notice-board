package se.experis.notisboard.models;

public class CommentDisplayModel {
    public String commentId;
    public String commentText;
    public String author;
    public String date;
}
