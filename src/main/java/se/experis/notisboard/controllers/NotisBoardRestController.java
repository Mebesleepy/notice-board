package se.experis.notisboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import se.experis.notisboard.Repositories.CommentRepository;
import se.experis.notisboard.Repositories.PostRepository;
import se.experis.notisboard.Repositories.UserRepository;
import se.experis.notisboard.models.Author;
import se.experis.notisboard.models.Comment;
import se.experis.notisboard.models.Post;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@RestController
public class NotisBoardRestController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private PostRepository postRepository;


    /////////////////////////////////////    USERS     ///////////////////////////////////////////////

    @GetMapping("/user")
    public ResponseEntity<List<Author>> getAllUsers() {

       ArrayList<Author> allUsers = (ArrayList<Author>) userRepository.findAll();

        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(allUsers, resp);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Author> getUser(HttpServletRequest request, @PathVariable Integer id) {

        Author user;
        HttpStatus resp;

        if (userRepository.existsById(id)) {
            System.out.println("User with id: " + id);
            user = userRepository.getById(id);
            resp = HttpStatus.OK;

        } else {
            System.out.println("User not found");
            user = null;
            resp = HttpStatus.NOT_FOUND;
        }
        ;

        return new ResponseEntity<>(user, resp);
    }

    @PostMapping("/user")
    public ResponseEntity<Author> adduser(@RequestBody Author user) {

        user = userRepository.save(user);

        System.out.println("new user with id: " + user.userName);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(user, resp);
    }


    /////////////////////////////   POSTS   //////////////////////////////////////////////

    @GetMapping("/post")
    public ResponseEntity<List<Post>> getAllPosts() {

        ArrayList<Post> allPosts = (ArrayList<Post>) postRepository.findAll();

        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(allPosts, resp);
    }

    @GetMapping("/post/{id}")
    public ResponseEntity<Post> getPost(@PathVariable Integer id) {

        Post post;
        HttpStatus resp;

        if (postRepository.existsById(id)) {
            System.out.println("Post with id: " + id);
            post = postRepository.getById(id);
            resp = HttpStatus.OK;

        } else {
            System.out.println("post not found");
            post = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(post, resp);
    }

    @PostMapping("/post")
    public ResponseEntity<Post> createPost(@RequestBody Post post) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        post.date = dtf.format(now);

        post = postRepository.save(post);

        System.out.println("new post with id: " + post.id);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(post, resp);
    }

    @DeleteMapping("/post/{id}")
    public ResponseEntity<String> deletePost(@PathVariable Integer id) {

        postRepository.deleteById(id);

        HttpStatus resp = HttpStatus.OK;

        return new ResponseEntity<>("Deleted", resp);
    }


    // Todo make patch....
    @PatchMapping("/post")
    public ResponseEntity<String> patchPost(@RequestBody Post post) {

        Post oldPost = postRepository.findById(post.id).get();

        oldPost.text = post.text;

        postRepository.save(oldPost);

        HttpStatus resp = HttpStatus.OK;

        return new ResponseEntity<>("patched", resp);
    }

    /////////////////////////   Comments   ///////////////////////////////////////////


    // get all comments
    @GetMapping("/comment")
    public ResponseEntity<List<Comment>> getAllComments() {

        ArrayList<Comment> allComments = (ArrayList<Comment>) commentRepository.findAll();

        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(allComments, resp);
    }

    // get a comment by id
    @GetMapping("/comment/{id}")
    public ResponseEntity<Comment> getComment(@PathVariable Integer id) {

        Comment comment;
        HttpStatus resp;

        if (commentRepository.existsById(id)) {
            System.out.println("Post with id: " + id);
            comment = commentRepository.getById(id);
            resp = HttpStatus.OK;

        } else {
            System.out.println("post not found");
            comment = null;
            resp = HttpStatus.NOT_FOUND;
        }

        return new ResponseEntity<>(comment, resp);
    }

    // create a comment based on the post it is commenting on
    @PostMapping("/comment/{id}")
    public ResponseEntity<Comment> createComment(@PathVariable Integer id, @RequestBody Comment comment) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();

        comment.date = dtf.format(now);

        Post post = postRepository.findById(id).get();

        post.comments.add(comment);
        comment.post = post;

        postRepository.save(post);
        commentRepository.save(comment);

        System.out.println("new comment with id: " + comment.id);

        HttpStatus resp = HttpStatus.CREATED;

        return new ResponseEntity<>(comment, resp);
    }

    @DeleteMapping("/comment/{id}")
    public ResponseEntity<String> deleteComment(@PathVariable Integer id) {

        commentRepository.deleteById(id);

        HttpStatus resp = HttpStatus.OK;

        return new ResponseEntity<>("Deleted", resp);
    }
}
