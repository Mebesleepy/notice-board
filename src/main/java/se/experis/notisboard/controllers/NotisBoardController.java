package se.experis.notisboard.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import se.experis.notisboard.Repositories.CommentRepository;
import se.experis.notisboard.Repositories.PostRepository;
import se.experis.notisboard.Repositories.UserRepository;
import se.experis.notisboard.models.Comment;
import se.experis.notisboard.models.CommentDisplayModel;
import se.experis.notisboard.models.Post;
import se.experis.notisboard.models.PostDisplayModel;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
public class NotisBoardController {

    @Autowired
    PostRepository postRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    UserRepository userRepository;

    /*
     * Your run to the mill standard paths, with thymeleaf providing data. Could do some cleaning up and
     * place duplicate code in separate functions for a more DRY result. the initial plan was to have separate
     * html pages for logged in and not logged in, but scraped that idea.
     */

    @GetMapping("/")
    public String postsPage(@CookieValue(value = "userId", defaultValue = "undefined") String userId, Model model) {

        List<PostDisplayModel> postDisplay = new ArrayList<>();

        List<Post> posts = postRepository.findAll(Sort.by(Sort.Direction.DESC, "date"));

        for (Post post : posts) {
            PostDisplayModel pdm = new PostDisplayModel();

            pdm.authorId = post.authorId;
            pdm.authorName = userRepository.getById(Integer.parseInt(pdm.authorId)).userName;
            pdm.postDate = post.date;
            pdm.postText = post.text;
            pdm.postId = Integer.toString(post.id);
            pdm.commentsAmount = post.comments.size();

            postDisplay.add(pdm);
        }

        model.addAttribute("userid", userId);

        model.addAttribute("postDisplay", postDisplay);

        return "posts";
    }


    @GetMapping("/post/myposts/{id}")
    public String myPosts(@PathVariable Integer id, Model model) {

        List<PostDisplayModel> postDisplay = new ArrayList<>();

        List<Post> posts = postRepository.findAll();

        for (Post post : posts) {

            if (post.authorId.equals(Integer.toString(id))) {

                PostDisplayModel pdm = new PostDisplayModel();

                pdm.authorId = post.authorId;
                pdm.authorName = userRepository.getById(Integer.parseInt(pdm.authorId)).userName;
                pdm.postDate = post.date;
                pdm.postText = post.text;
                pdm.postId = Integer.toString(post.id);
                pdm.commentsAmount = post.comments.size();

                postDisplay.add(pdm);
            }
        }

        model.addAttribute("postDisplay", postDisplay);

        return "myposts";
    }

    @GetMapping("/post/edit/{id}")
    public String editPage(@PathVariable Integer id, Model model) {


        Post post = postRepository.findById(id).get();

        model.addAttribute("postId", post.id);
        model.addAttribute("postText", post.text);

        return "editPost";
    }


    @GetMapping("/login")
    String login() {
        return "login";
    }


    @GetMapping("/comments/{id}")
    public String commentsPage(@PathVariable Integer id, Model model) {
        List<CommentDisplayModel> comments = new ArrayList<>();
        PostDisplayModel pdm = new PostDisplayModel();

        Post post = postRepository.findById(id).get();

        for (Comment comment : post.comments) {
            CommentDisplayModel cdm = new CommentDisplayModel();

            cdm.author = userRepository.getById(Integer.parseInt(comment.author)).userName;
            cdm.commentId = Integer.toString(comment.id);
            cdm.commentText = comment.text;
            cdm.date = comment.date;

            comments.add(cdm);
        }

        pdm.authorId = post.authorId;
        pdm.authorName = userRepository.getById(Integer.parseInt(pdm.authorId)).userName;
        pdm.postDate = post.date;
        pdm.postText = post.text;
        pdm.postId = Integer.toString(post.id);

        model.addAttribute("postDisplay", pdm);
        model.addAttribute("comments", comments);

        return "comments";
    }

//     if a user successfully logs in, set cookie with user id and set time limit to 1o minutes.
//    That was the plan anyways but i did not get why i could not get the cookie.
    @GetMapping("/setCookie/{id}")
    public String setCookie(HttpServletResponse response, @PathVariable Integer id) {

        Cookie newCookie = new Cookie("userId", Integer.toString(id));

        newCookie.setMaxAge(600);

        response.addCookie(newCookie);

        return "redirect:/";
    }


}