package se.experis.notisboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotisBoardApplication {

	public static void main(String[] args) {
		SpringApplication.run(NotisBoardApplication.class, args);
	}

}
